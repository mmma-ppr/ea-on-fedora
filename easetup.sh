#!/bin/bash
set -v
# Copyright © 2021 wanderer <wanderer@git.dotya.ml>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See http://www.wtfpl.net/ for more details.

#   this is just a really dumb EA setup on fedora(33) through wine

#   what we'll do in order to get the $hit to work:
#       register a winehq repo
#       install the most recent winehq-devel from winehq repo
#       get winetrics installer from github (binary in fedora repos
#       fails to work and is outdated)
#       chmod +x it and move it to /usr/bin and chown root:root it
#       run ea-recommended winetricks commands
#       reboot
#       curl -O EA && install easetup.msi
#       done.

  # verify sufficient privileges...
  if [ $(id -u) != 0 ]; then
    echo "  [x] eeeeee, not root. exiting..."
    exit 183287
  fi

setVars() {
  # set these values so the installer can run in color
  COL_NC='\e[0m' # no color
  COL_LIGHT_GREEN='\e[1;32m'
  COL_LIGHT_RED='\033[1;31m'
  COL_PURPLE='\e[1m\033[35m' # bold purple
  COL_LIGHT_BLUE='\e[1;34m'
  COL_LIGHT_YELLOW='\e[1m\e[3;33m' # italic bold, used for warning
  TICK="[${COL_LIGHT_GREEN}✓${COL_NC}]"
  CROSS="[${COL_LIGHT_RED}✗${COL_NC}]"
  INFO="[${COL_LIGHT_BLUE}i${COL_NC}]"
  WARNING="${COL_LIGHT_YELLOW}[!]" # put "no color" at the end of the warning!
  #shellcheck disable=SC2034
  DONE="${COL_LIGHT_GREEN}done! ${COL_NC}"
  OVER="\\r\\033[K"
  NP_USR="$SUDO_USER" # will be using this to run usr commands
}


unsetVars() {
  unset COL_NC
  unset COL_LIGHT_GREEN
  unset COL_LIGHT_RED
  unset COL_PURPLE
  unset COL_LIGHT_BLUE
  unset COL_LIGHT_YELLOW
  unset TICK
  unset CROSS
  unset INFO
  unset WARNING
  unset DONE
  unset OVER
  unset NP_USR
  unset pkgmgr
  unset relll
}

introduction() {
  echo "      +---------------------+"
  echo "      |   ea through wine   |"
  echo "      +---------------------+"
  echo
  echo "  [*] hello ${NP_USR}! this is an ea-through-wine installer script"
  echo "  [*] we've got some work to do in order to get this \$h*t working..."
  echo "  [*] i.e you're going to need a lot of bandwidth :D"
  echo

  #NP_USR="$SUDO_USER" # will be using this to run usr commands
  echo -e "  [*] so, let's start off by letting you know that ${COL_LIGHT_RED}you've just been pwnd${COL_NC}..."
  echo -e "  [${COL_LIGHT_GREEN}\$${COL_NC}] EVERYTHING YOU OWN NOW BELONGS TO US!"
  echo -e "      that was supposed to be a joke ofc "
  echo -e "  [*]${COL_LIGHT_RED} chargin' mah lazer${COL_NC}"
}


  # first, let's check it's Fedora we're running on...
isFedora() {
  export relll="/etc/redhat-release"
  if [ ! -f "$relll" ]; then
        echo -e "  ${CROSS} only ${COL_LIGHT_BLUE}Fedora/CentOS are supported${COL_NC} at the moment, exiting..."
         unsetVars && exit 9807362
  fi

  echo -e "  ${INFO} checking platform..."
  # centos not tested
  #if [ (grep -iq 'fedora' $relll) || (grep -iq 'centos' $relll) ]; then 
    echo -e "  ${TICK} neat. working on a ${COL_PURPLE}$(cat $relll) ${COL_NC} [hint: supported]"
    echo -e "  ${INFO} some more detail about you" #cat /proc/version also possible
    echo "       kernel: v`uname -r`"
    echo "       platform: `uname -i`"
    echo "`command free -m | head -2`"
  #fi
}


wineRepos() {
        if grep -iq 'winehq.org/wine-builds' /etc/yum.repos.d/* ;then
                echo -e "  ${TICK} winehq repository already registered, skipping"
                echo
        else
                echo -e "  ${INFO} registering a winehq repository"
        fi
        if grep -iq 'fedora' $relll ; then
                export pkgmgr=dnf
                #$pkgmgr clean all
                $pkgmgr config-manager --add-repo https://dl.winehq.org/wine-builds/fedora/33/winehq.repo
                if [ `echo $?` -ne 0 ]; then
                        echo -e "  ${CROSS} failed to register winehq repository, aborting..."
                        unsetVars && exit 1
                fi
        elif grep -iq 'centos' $relll ; then
                export pkgmgr=yum
                $pkgmgr clean metadata
                rpm -Uhv https://dl.winehq.org/wine-builds/fedora/33/winehq.repo
                if [ `echo $?` -ne 0 ]; then
                        echo -e "  ${CROSS} failed to register winehq repository, aborting..."
                        unsetVars && exit 1
                fi
        fi

        echo -e "  ${TICK} winehq repository successfully registered"
}


upgradePkgCache() {
  local str="refreshing pkg cache"
  echo -e "  ${INFO} ${str}"
  $pkgmgr -y upgrade || dilemmaFailedPkgCacheUpgrade
  echo -e "  ${TICK} pkg cache refreshed"
  echo
}

dilemmaFailedPkgCacheUpgrade() {
  if [ `echo $?` -ne 0 ]; then
    echo -e "  ${WARNING} failed to refresh cache ${COL_NC}"
    echo -e "  ${INFO} we need to have the latest available packages"
    echo -e "  ${WARNING} if you are fairly sure you're up-to-date... "
    echo -e "      do you want to continue without upgrading now?${COL_NC}"
    while true; do
      read -p "                          (y/N): " yn
        case $yn in
          ([yY]*) 
          echo -e "  ${WARNING} proceeding with the installation ${COL_NC}";;
          ([nN]*) echo -e "  ${CROSS} exiting..."
          unsetVars && exit 3245453;;
          (*) echo "please answer yes or no";;
        esac
    done
  fi
  echo
}


wineAndFonts() {
  if [ `rpm -q winehq-devel > /dev/null 2>&1; echo $?` -eq 0 ]; then
    echo -e "  ${TICK} winehq-devel is already installed, skipping"
  else
    echo -e "  ${INFO} not installed, so installing winehq-devel"
    $pkgmgr -y install winehq-devel > /dev/null 2>&1
    if [ `echo $?` -ne 0 ]; then
      echo -e "  ${CROSS} failed to install winehq-devel, aborting..."
      echo -e "      we would not have much fun without that pkg either..."
      unsetVars && exit 1
    else
      echo -e "  ${TICK} winehq-devel successfully installed"
    fi
  fi

  if [ `rpm -q texlive-carlito > /dev/null 2>&1; echo $?` -eq 0 ]; then
    echo -e "  ${TICK} carlito compat font is already installed, skipping"
    echo
  else
    echo -e "  ${INFO} not installed, so installing texlive-carlito (ea compat font)"
    $pkgmgr -y install texlive-carlito > /dev/null 2>&1
    if [ `echo $?` -ne 0 ]; then
      echo -e "  ${CROSS} failed to install compat font, aborting... \n    fonts for win\$hit could be an issue"
      unsetVars && exit 1
    else
      echo -e "  ${TICK} carlito compatibility font successfully installed"
      echo
    fi
  fi
}


latest_update_winetricks() {
  echo -e "  ${INFO} setting up winetricks upgrade script"
  cd "$(mktemp -d)"

cat > update_winetricks <<_EOF_SCRIPT
#!/bin/sh

cd "$(mktemp -d)"
curl -O https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks
curl -O https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks.bash-completion
chmod +x winetricks
grep w_set_winver winetricks
sudo sh -c 'mv winetricks /usr/bin ; mv winetricks.bash-completion /usr/share/bash-completion/completions/winetricks'
_EOF_SCRIPT

  if [ $(echo $?) -ne 0 ]; then
    echo -e "  ${CROSS} error setting up \"update_winetricks\" file"
    unsetVars && exit 1
  else
    echo -e "  ${TICK} setting up winetricks upgrade script"
  fi
}


latestWinetricksSelf() {
  local str="fixing permissions and moving your dear files all around"
  echo -e "  ${INFO} ${str}"
  chmod 0755 update_winetricks || echo errrrr
  chmod +x update_winetricks || echo noPerms
  if [ $(echo $?) -ne 0 ]; then
    echo -e "  ${CROSS} there was an issue setting the correct permissions on update_winetricks, aborting..."
    unsetVars && exit 1
  else
    echo -e "  ${TICK} setting the correct permissions on \"update_winetricks\""
  fi

  mv update_winetricks /usr/bin/ || echo neviiim
  if [ `echo $?` -ne 0 ]; then
    echo -e "  ${CROSS} there was an issue moving \"update_winetricks\" to /usr/bin, aborting..."
    unsetVars && exit 1
  else
    echo -e "  ${TICK} moving \"update_winetricks\" to /usr/bin"
  fi
  #ls -la /usr/bin/update_winetricks
  #chmod 0755 /usr/bin/update_winetricks || echo errrrooor
  echo -e "  ${TICK} done ${str}"

  echo -e "  ${INFO} getting us latest winetricksthemselvesbro"
  bash -c '/usr/bin/update_winetricks > /dev/null 2>&1' > /dev/null 2>&1
  if [ $(echo $?) -ne 0 ]; then
    echo -e "  ${CROSS} there was an issue with getting/setting up latest winetricksthemselvesbro, aborting..."
    unsetVars && exit 1
  fi

  echo -e "  ${TICK} setting the correct permissions on winetricksthemselvesbro"
  chmod 0755 /usr/bin/winetricks
  if [ $(echo $?) -ne 0 ]; then
    echo -e "  ${CROSS} there was an issue setting the correct permissions on winetricksthemselvesbro, aborting..."
    unsetVars && exit 1  
  else
    echo -e "  ${TICK} latest winetricksthemselvesbro is all set"
  fi
}

winetricksPkgs() {
  local str="installing ea winetricks dep-packages" 
  echo -e "  ${INFO} ${str}"
  echo -e "  ${WARNING} user assistance may be needed for next few steps ${COL_NC}"


  echo -e "  ${INFO} attempting to install msxml3"
  su $NP_USR bash -c 'winetricks -q msxml3 > /dev/null 2>&1' > /dev/null 2>&1
  if [ $(echo $?) -ne 0 ]; then
    echo -n "exit code is "; echo $?
    echo -e "  ${CROSS} there's been an error getting msxml3 ready"
    unsetVars && exit 1
  else
    echo -e "  ${TICK} got dat msxml3 [winetricks -q msxml3]"
  fi


  echo -e "  ${INFO} attempting to install msxml4"
  su $NP_USR bash -c 'winetricks -q msxml4 > /dev/null 2>&1' > /dev/null 2>&1
  if [ $(echo $?) -ne 0 ]; then
    echo -n "exit code is "; echo $?
    echo -e "  ${CROSS} there's been an error getting msxml4 ready"
    unsetVars && exit 1
  else
    echo -e "  ${TICK} got dat msxml4 [winetricks -q msxml4]"
  fi


  echo -e "  ${INFO} attempting to install mdac28"
  su $NP_USR bash -c 'winetricks -q mdac28 > /dev/null 2>&1' > /dev/null 2>&1
  echo
  if [ $(echo $?) -ne 0 ]; then
    echo -n "exit code is "; echo $?
    echo -e "  ${CROSS} there's been an error getting mdac28 ready"
    unsetVars && exit 1
  else
    echo -e "  ${TICK} got dat mdac28 [winetricks -q mdac28]"
  fi


  echo -e "  ${INFO} attempting to install refresh winetricks cache [winetricks -q --force mdac28]"
  su $NP_USR bash -c 'winetricks -q --force mdac28 > /dev/null 2>&1' > /dev/null 2>&1
  echo
  if [ $(echo $?) -ne 0 ]; then
    echo -n "exit code is "; echo $?
    echo -e "  ${CROSS} there's been an error executing \"winetricks -q --force mdac28\""
    unsetVars && exit 1
  else
    echo -e "  ${TICK} dat winetricks -q --force mdac28 run successfully"
  fi

  echo -e "  ${TICK} ${str}"
}


eaInstall() {
  echo -e "  ${INFO} getting an ea msi and executing it [\e[1m\e[35mthat's the thing\e[0m]"
  echo -e "  ${WARNING} user assistance may be needed ${COL_NC}"

  echo -e "  ${INFO} attempting to install ea"
  cat > installme <<_EOF_SCRIPT
#!/bin/sh

cd "$(mktemp -d)"
curl -O https://www.sparxsystems.com/bin/easetup.msi 
echo
export WINEARCH=win64
wine msiexec /i easetup.msi /q > /dev/null 2>&1
winetricks -q --force mdac28 > /dev/null 2>&1
wine start .wine/drive_c/users/$USER/Desktop/Enterprise\ Architect.lnk & > /dev/null 2>&1
echo
_EOF_SCRIPT

  chmod +x installme || echo "issue with perms of installme"
  chown -Rv "$NP_USR":"$NP_USR" .  || echo "issue with ownership of installme"
  echo -e "  ${TICK} perms and ownership of installme successfully set up"

  echo -e "  ${INFO} downloading the internet"
  bash -c "su $NP_USR bash -c './installme'" > /dev/null 2>&1 #let's not see curl

  if [ $(echo $?) -ne 0 ]; then
    echo -e "  ${CROSS} error getting \"easetup.msi\" file"
    unsetVars && exit 1
  else
    echo -e "  ${TICK} easetup.msi successfully installed"
  fi

}

goodbye() {
  echo -e "  ${TICK} you should be all set now"
  echo -e "      ${DONE}"
}


main() {
  setVars
  introduction
  isFedora
  wineRepos
  upgradePkgCache
  wineAndFonts
  latest_update_winetricks
  latestWinetricksSelf
  winetricksPkgs
  eaInstall
  goodbye
  unsetVars
}

  main

  # some docs
  #
  # ea reference sheet
  # https://www.sparxsystems.com/enterprise_architect_user_guide/14.0/product_information/enterprise_architect_linux.html
  # winehq fedora site
  # https://wiki.winehq.org/Fedora
  # winetricks reference page
  # https://wiki.winehq.org/Winetricks
  # winetricks github repo
  # https://github.com/Winetricks/winetricks
  # http://www.wtfpl.net/

